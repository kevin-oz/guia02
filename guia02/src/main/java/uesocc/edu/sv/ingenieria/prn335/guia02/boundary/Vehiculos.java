/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uesocc.edu.sv.ingenieria.prn335.guia02.boundary;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.servlet.http.Part;
import uesocc.edu.sv.ingenieria.prn335.guia02.controller.FrmServlet;

/**
 *
 * @author kevin Figueroa
 */
@Stateless
@LocalBean
public class Vehiculos implements Serializable{
    
     public static final String VALIDADOR_FORMATO_PLACA_UNA_LETRA="[PONACVTMDE][ ]{1,3}[0-9]{1,3}-[0-9]{3}";
     public static final String FILTRA_P="[P][ ]{1,3}[0-9]{1,3}-[0-9]{3}";
     public static final String VALIDADOR_FORMATO_PLACA_DOS_LETRAS=
             "[A][B][ ]{1,3}[0-9]{1,3}-[0-9]{3}|"
             + "[C][D][ ]{1,3}[0-9]{1,3}-[0-9]{3}"
             + "|[P][R][ ]{1,3}[0-9]{1,3}-[0-9]{3}" 
             + "|[M][B][ ]{1,3}[0-9]{1,3}-[0-9]{3}"
             + "|[R][E][ ]{1,3}[0-9]{1,3}-[0-9]{3}";
     public static final String VALIDADOR_FORMATO_PLACAS="[\\w]{1,2}[ ]{1,3}[0-9]{1,3}-[0-9]{3}";
   
    /**
     * 
     * @param part_file un archivo de partes.
     * @return Lista tipo String con con el contenido del archivo subido.
     */
    public List<String> crearLista(Part part_file) {
        List<String> placas= new ArrayList<>();
        if (part_file!= null && part_file.getSize() > 0) { 
            InputStream contenido;
            try {
                contenido = part_file.getInputStream(); 

                try (BufferedReader leer = new BufferedReader(new InputStreamReader(contenido))) {
                    String line;
                    while ((line = leer.readLine()) != null) {
                        placas.add(line);
                    }
                }
            } catch (IOException e) {
                Logger.getLogger(FrmServlet.class.getName()).log(Level.SEVERE, null, e);
            }

        }
        return placas;
    }
    
    /**
     * 
     * @param placas Lista tipo String que contine todas las placas del archivo subido
     * @return Lista tipo String con las placas que poseen un formato válido
     */
    public List<String> placasValidas(List<String> placas){
         String cadena="";
        for (String placa : placas) {
            if(placa.trim().matches(VALIDADOR_FORMATO_PLACA_UNA_LETRA) || placa.trim().matches(VALIDADOR_FORMATO_PLACA_DOS_LETRAS)){
                cadena=cadena+","+placa;
            }
        }
      List<String> myList = new ArrayList<>(Arrays.asList(cadena.split(",")));
        return myList;
    }
    
    /**
     * 
     * @param listaPlacas Lista tipo String que contine todas las placas del archivo subido
     * @return Lista tipo String con unicamete las placas del tipo P (particulares) con un formato diferente al original;
     * se reemplaza el guion (-) por una barra lateral (/).
     * 
     */
public List<String> cambiarFormatoPlacasP(List<String> listaPlacas){
    
    String salida="",cambioFormato;
    for (String listaPlaca : listaPlacas) {
        if(listaPlaca.trim().matches(FILTRA_P)){
            salida=salida+","+listaPlaca;
        }
    }
    cambioFormato=salida.replaceAll("-","/");
        List<String> listaPlacasP = new ArrayList<>(Arrays.asList(cambioFormato.split(",")));
    
        
        
    return listaPlacasP; 
}

    
    
    
    
}
